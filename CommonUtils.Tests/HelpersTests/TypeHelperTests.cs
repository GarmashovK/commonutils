﻿using Algorithms.Helpers;
using NUnit.Framework;

namespace CommonUtils.Tests.HelpersTests
{
    [TestFixture()]
    public class TypeHelperTests
    {
        [Test]
        public void IsDefaultTest()
        {
            Assert.IsFalse(1.IsDefault()); //False
            Assert.IsFalse(0.IsDefault()); // True

            // reference type
            Assert.IsFalse("test".IsDefault()); //False
            Assert.IsTrue(((string)null).IsDefault()); //True //True
        }
        [Test]
        public void IsNotDefaultTest()
        {
            Assert.IsTrue(1.IsNotDefault()); //False
            Assert.IsTrue(0.IsNotDefault()); // True

            // reference type
            Assert.IsTrue("test".IsNotDefault()); //False
            Assert.IsFalse(((string)null).IsNotDefault()); //True //True
        }

        [Test]
        public void IsValueTypeTest()
        {
            Assert.IsTrue(1.IsValueType());
            Assert.IsFalse("test".IsValueType());
        }
    }
}