﻿using System.Linq;
using CommonUtils.Helpers;
using MoreLinq;
using NUnit.Framework;

namespace CommonUtils.Tests.HelpersTests
{
    [TestFixture]
    public class CollectionHelperTests
    {
        [Test]
        public void CreateListTest()
        {
            var n = 20;
            var item = new object();

            var list = CollectionHelper.CreateList(() => item, n);

            Assert.AreEqual(n, list.Count());
            list.ForEach(i => Assert.AreEqual(item, i));
        }

        [Test]
        public void ContainsTest()
        {
            var items = new[] {new object(), new object()};

            Assert.IsTrue(items.Contains(i => i == items[0]));
            Assert.IsFalse(items.Contains(i => i == new object()));
        }
    }
}