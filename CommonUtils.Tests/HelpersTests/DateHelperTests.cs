﻿using System;
using CommonUtils.Helpers;
using NUnit.Framework;

namespace CommonUtils.Tests.HelpersTests
{
    [TestFixture]
    public class DateHelperTests
    {
        [Test]
        public void GetStringInfoTest()
        {
            var timespan = TimeSpan.FromSeconds(61);

            Assert.AreEqual("1m 1s", timespan.GetStringInfo());
        }
    }
}