﻿using System;
using CommonUtils.Helpers;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;

namespace CommonUtils.Tests.HelpersTests
{
    [TestFixture]
    public class JsonHelperTests
    {
        public class TestDto
        {
            public string Prop1 { get; private set; } = "Prop1";
            public decimal Prop2 { get; private set; } = 2.333m;
            public DateTime CreationTime { get; private set; } = DateTime.UtcNow;

            public TestDto() { }
        }

        [Test]
        public void GetJsonTest()
        {
            var dto = new TestDto();

            var json = dto.GetJson();

            var expected = JsonConvert.SerializeObject(dto, JsonHelper.Settings);
            Assert.AreEqual(expected, json);
        }

        [Test]
        public void DeserializeJsonTest()
        {
            var dto = new TestDto();

            var json = dto.GetJson();
            var recoveredField = json.DeserializeJson<TestDto>();

            dto.Should().BeEquivalentTo(recoveredField, opts => opts
                .Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, 1))
            .WhenTypeIs<DateTime>());
        }
    }
}