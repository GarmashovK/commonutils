﻿using CommonUtils.Helpers;
using NUnit.Framework;

namespace CommonUtils.Tests.HelpersTests
{
    [TestFixture]
    public class StringHelpersTests
    {
        [Test]
        public void IsMobilePhoneTest()
        {
            var phoneWithPlus7 = "+79215851340";
            Assert.IsTrue(phoneWithPlus7.IsMobilePhone());

            var phoneWith8 = "89215851343";
            Assert.IsTrue(phoneWith8.IsMobilePhone());

            var phoneWith7 = "79215851340";
            Assert.IsTrue(phoneWith7.IsMobilePhone());

            var notPHone = "48237408098098";
            Assert.IsFalse(notPHone.IsMobilePhone());
        }

        [Test]
        public void RemoveNotNumbersTest()
        {
            var phone = "+7 (922) 333 44 55";
            var expected = "79223334455";

            Assert.AreEqual(expected, phone.RemoveNotNumbers());
        }

        [TestCase("+79212223344", ExpectedResult = "79212223344")]
        [TestCase("9212223344", ExpectedResult = "79212223344")]
        public string RemoveNotNumbersTest(string phone)
        {
            return phone.FormatPhone();
        }

        [TestCase("hello__hello_hello", ExpectedResult = @"hello\_\_hello\_hello")]
        public string ReplaceMarkdownUnderscorsTest(string str)
        {
            return str.ReplaceMarkdownUnderscors();
        }
    }
}