﻿using CommonUtils.Helpers;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests.ExtensionsTests
{
    [TestFixture]
    public class DecimalExtensionsTests
    {
        [Test]
        public void ToStringWithClearEndNullsTest()
        {
            decimal manyNulls = 0.000000000000000m;
            var formatedDecimal = manyNulls.ToStringWithClearEndNulls();
            Assert.AreEqual("0",formatedDecimal );

            decimal number = 0.03456000000m;

            Assert.AreEqual("0.03456", number.ToStringWithClearEndNulls());

            decimal numberWihtoutPoint = 3500m;

            Assert.AreEqual("3500", numberWihtoutPoint.ToStringWithClearEndNulls());
        }
    }
}