﻿using CommonUtils.Helpers;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests.ExtensionsTests
{
    [TestFixture()]
    public class StringExtensionsTest
    {
        [Test]
        public void IsMobilePhoneTest()
        {
            var phoneWith7 = "+79215851340";
            var phoneWithoutPlus = "79215851340";
            var phoneWith8 = "89215851340";

            Assert.IsTrue(phoneWith8.IsMobilePhone());
            Assert.IsTrue(phoneWith7.IsMobilePhone());
            Assert.IsTrue(phoneWithoutPlus.IsMobilePhone());
            Assert.IsFalse("83890123854092380".IsMobilePhone());
        }
    }
}