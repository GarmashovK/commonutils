﻿using System;
using CommonUtils.Common.Jobs;
using CommonUtils.Common.Logger;

namespace CommonUtils.Tests.CommonTests.Jobs
{
    /// <summary>
    /// Периодическая задача
    /// </summary>
    internal class JobMyPeriodicalTest : JobBaseScheduled
    {
        public bool getDelayFlag;

        internal JobMyPeriodicalTest(Action action, TimeSpan startDelay) : base(action, startDelay, new FakeLogger())
        {
        }

        protected override TimeSpan GetDelay()
        {
            getDelayFlag = true;
            //var now = DateTime.Now;
            //var f = now.RoundUp(TimeSpan.FromMinutes(30));
            return TimeSpan.FromMinutes(1);
        }
    }
}
