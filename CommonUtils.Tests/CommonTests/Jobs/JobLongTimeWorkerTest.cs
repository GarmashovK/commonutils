﻿using System;
using System.Threading;
using CommonUtils.Common.Jobs;
using CommonUtils.Common.Logger;

namespace CommonUtils.Tests.CommonTests.Jobs
{
    /// <summary>
    /// Долго выполнящаяся задача
    /// </summary>
    internal class JobLongTimeWorkerTest : JobBaseScheduled
    {
        public JobLongTimeWorkerTest(TimeSpan startDelay, ILogger logger) : base(startDelay, logger)
        {
        }

        protected override TimeSpan GetDelay()
        {
            return TimeSpan.FromSeconds(60);
        }
        protected override Action GetAction()
        {
            return Perform;
        }

        private void Perform()
        {
            for (int i = 0; i < 10; i++)
            {
                if (cancelToken.IsCancellationRequested)
                    return;
                Thread.Sleep(1000);
            }
            
        }

    }
}
