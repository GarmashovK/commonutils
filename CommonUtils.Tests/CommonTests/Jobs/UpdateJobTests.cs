﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CommonTestsUtils.Abstractions;
using CommonUtils.Common.Jobs;
using CommonUtils.Common.Jobs.Interfaces;
using Moq;
using MoreLinq;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests.Jobs
{
    [TestFixture]
    public class UpdateJobTests : TestWithLogger
    {
        private TimeSpan interval = TimeSpan.FromMilliseconds(100);
        private TimeSpan delaySpan = TimeSpan.FromTicks(1);
        private IEnumerable<Mock<IUpdateModule>> updateModules;
        private UpdateJob updateJob;

        [SetUp]
        public void Setup()
        {
            updateModules = GetUpdateModules();
            updateJob = new UpdateJob(updateModules.Select(u =>u.Object), interval, delaySpan, logger);
        }

        private IEnumerable<Mock<IUpdateModule>> GetUpdateModules(int n = 5)
        {
            var modules = new List<Mock<IUpdateModule>>();

            for(var i=0; i<n; i++)
                modules.Add(GetNewUpdateModule());

            return modules;
        }

        private Mock<IUpdateModule> GetNewUpdateModule()
        {
            var updateModule = new Mock<IUpdateModule>();

            updateModule.Setup(o => o.Update()).Verifiable();

            return updateModule;
        }

        [Test]
        public void ConstrucotrTest()
        {
            Assert.Throws<ArgumentNullException>(() =>
                new UpdateJob(null, interval, delaySpan, logger));
            Assert.Throws<ArgumentNullException>(() =>
                new UpdateJob(new []{ null, GetNewUpdateModule().Object}, interval, delaySpan, logger));

            Assert.IsInstanceOf<JobActionScheduled>(updateJob);
        }

        [Test]
        public void UpdateActionTest()
        {
            updateJob.Start();
            Thread.Sleep(interval / 2);
            updateModules.ForEach(module => module.Verify(o => o.Update(), Times.Once));
            const int repeat = 5;
            for (var i = 0; i < repeat; i++)
            {
                Thread.Sleep(interval);
                updateModules.ForEach(module => module.Verify(o => o.Update(), Times.Exactly(i + 2)));
            }
        }
    }
}