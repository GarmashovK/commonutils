﻿using System;
using CommonUtils.Common.Jobs;
using CommonUtils.Common.Logger;

namespace CommonUtils.Tests.CommonTests.Jobs
{
    /// <summary>
    /// Однократно выполняемая задача
    /// </summary>
    internal class JobMyOneTimesTest : JobBaseScheduled
    {
        internal JobMyOneTimesTest(Action action, TimeSpan startDelay) : base(action, startDelay, new FakeLogger())
        {
        }
    }
}
