﻿using System;
using System.Threading.Tasks;
using CommonUtils.Common.Jobs;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests.Jobs
{
    [TestFixture]
    public class JobBaseScheduledTests
    {
        [Test]
        public void ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new JobMyPeriodicalTest(null, TimeSpan.MinValue));
            Assert.DoesNotThrow(() => new JobMyPeriodicalTest(() => { }, TimeSpan.MinValue));
        }
        [Test]
        public void PeriodicalJobTest()
        {
            var i = 1;
            var job = new JobMyPeriodicalTest(() => i++, TimeSpan.FromMilliseconds(1));
            Assert.True(job.JobState == JobStates.None);
            job.Start();
            Assert.True(job.JobState == JobStates.InProgress);
            Task.Delay(TimeSpan.FromMilliseconds(100)).Wait();
            job.Stop();
            Assert.True(job.JobState == JobStates.Stopped);
            Assert.True(i == 2);
            Assert.True(job.getDelayFlag);
        }

    }
}
