﻿using System;
using System.Threading;
using CommonUtils.Common.Cache;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests.CacheTests
{
    [TestFixture]
    public class LifetimeCacheTests
    {
        private TimeSpan lifeTime = TimeSpan.FromMilliseconds(100);

        [Test]
        public void ConstructorTest()
        {
            var cache = new LifetimeCache<int>(lifeTime);
            Assert.IsInstanceOf<ICache<int>>(cache);
        }

        [Test]
        public void GetCacheTest()
        {
            var number = 10;
            var cache = new LifetimeCache<int>(lifeTime);
            cache.Update(number);

            Assert.AreEqual(number, cache.GetCache());
            Thread.Sleep(lifeTime + lifeTime /2);
            Assert.Throws<Exception>(() => cache.GetCache());
        }
    }
}