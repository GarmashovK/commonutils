﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CommonUtils.Common;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests
{
    [TestFixture]
    public class IdGeneratorTests
    {
        [Test]
        [Ignore("CollisionTest")]
        public void GetNewOrderIdTest()
        {
            var ids = new List<string>();
            var times = 100;
            for (var i = 0; i < times; i++)
                ids.Add(IdGenerator.GetNewOrderId());

            var result = ids.Distinct();
            
            Assert.AreEqual(ids.Count, result.Count());
            Thread.Sleep(500);
            for (var i = 0; i < times; i++)
                ids.Add(IdGenerator.GetNewOrderId());

            Assert.AreEqual(times * 2, ids.Distinct().Count());
        }

        [Test]
        public void MillisecondsTest()
        {
            var now = DateTime.Now;
            var rand = new Random(now.Millisecond);
            var today = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            var lastSecondOfDay = new DateTime(now.Year, now.Month, now.Day, 
                rand.Next(1, 23), rand.Next(1, 59), rand.Next(1, 59));
            var dif = (lastSecondOfDay - today);
            var msToday = dif.TotalMilliseconds / 50;
        }
    }
}