﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using MoreLinq;
using NUnit.Framework;

namespace CommonUtils.Tests.CommonTests
{
    [TestFixture]
    public class CommonTests
    {
        private string feedbacksPath = "TestResources/Feedbacks.txt";
        private string feedbacksOutPath = "TestResources/Feedbacks.html";

        readonly DateTime startTime = new DateTime(2018, 2, 1);

        private class FeedbackItem
        {
            public string Name { get; set; }
            public DateTime CreationTime { get; set; }
            public string Body { get; set; }
        }

        private DateTime GetRandomDateTime()
        {
            Thread.Sleep(TimeSpan.FromTicks(10));
            var random = new Random((int)DateTime.Now.Ticks);
            var ts = TimeSpan.FromDays(random.Next(1, 365)) + new TimeSpan(random.Next(10, 22), random.Next(1, 59),0);
            var time = startTime + ts;

            return time;
        }

        [Test]
        [Ignore("GenerateFeedback")]
        public void GenerateDates()
        {
            var feedbacksFile = File.ReadLines(feedbacksPath, Encoding.ASCII);
            IEnumerable<FeedbackItem> feedbacks = CreateFeedbacks(feedbacksFile);

            File.WriteAllText(feedbacksOutPath, GetHtmlFeedbacks(feedbacks));
        }

        private string GetHtmlFeedbacks(IEnumerable<FeedbackItem> feedbacks)
        {
            var builder = new StringBuilder();
            
            feedbacks.ForEach(feedback => builder.AppendLine(GetHtmlFeedback(feedback)));
            
            return builder.ToString();
        }

        private string GetHtmlFeedback(FeedbackItem feedback)
        {
            var builder = new StringBuilder();

            builder.AppendLine("<div class='item'>");

            builder.AppendLine("<div class='name'>");
            builder.AppendLine(feedback.Name);

            builder.AppendLine($"<span>{feedback.CreationTime:dd MMM yyyy, HH:mm}</span>");
            builder.AppendLine("</div>");
            builder.AppendLine($"<p>{feedback.Body}</p>");

            builder.AppendLine("</div>");

            return builder.ToString();
        }

        private IEnumerable<FeedbackItem> CreateFeedbacks(IEnumerable<string> feedbacksFile)
        {
            var result = new List<FeedbackItem>();
            var n = feedbacksFile.Count();
            for (var i = 0; i < n; i+=2)
            {
                var feedbackItem = new FeedbackItem
                {
                    Name = feedbacksFile.ElementAt(i),
                    Body = feedbacksFile.ElementAt(i + 1),
                    CreationTime = GetRandomDateTime()
                };
                result.Add(feedbackItem);
            }

            return result.OrderByDescending(feedback => feedback.CreationTime);
        }
    }
}