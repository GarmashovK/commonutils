﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CommonUtils.Helpers
{
    public static class StringHelper
    {
        public static string ToHtml(this string str)
        {
            return Regex.Replace(str, @"(\r\n)|\n|\r", "<br/>");
        }

        public static bool IsMobilePhone(this string phoneString)
        {
            var regex = new Regex(@"^((\+?7)|8){1}\d{10}$");

            return regex.IsMatch(phoneString);
        }

        public static bool IsNotNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str) == false;
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static string RemoveNotNumbers(this string phone)
        {
            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(phone, "");
        }

        public static string FormatPhone(this string phone)
        {
            phone = phone.RemoveNotNumbers();
            if (phone.StartsWith("9") && phone.Length == 10)
                phone = $"7{phone}";
            return phone;
        }

        public static decimal TryParseNumber(this string strNumber)
        {
            return decimal.Parse(strNumber, 
                CultureInfo.InvariantCulture);
        }

        public static int ParseInt(this string strNumber)
        {
            return Convert.ToInt32(strNumber);
        }

        public static string ReplaceMarkdownUnderscors(this string str)
        {
            return str.Replace("_", @"\_");
        }
    }
}
