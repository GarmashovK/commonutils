﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace CommonUtils.Helpers
{
    public static class CollectionHelper
    {
        public static void ParallelForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            var processors = Environment.ProcessorCount;
            var count = collection.Count();

            if (count <= processors)
            {
                collection.ForEach(action.Invoke);
                return;
            }
            var processorParts = collection.Partition(count / processors);

            processorParts.AsParallel().ForEach(part =>
                part.ForEach(action.Invoke));
        }


        public static IEnumerable<TResult> ParallelSelect<TCollection, TResult>(this IEnumerable<TCollection> collection, Func<TCollection, TResult> selector)
        {
            var processors = Environment.ProcessorCount;
            var count = collection.Count();

            if (count <= processors)
            {
                return collection.Select(selector.Invoke);
            }
            var processorParts = collection.Partition(collection.Count() / processors);
            var result = new ConcurrentBag<TResult>();

            processorParts.AsParallel().ForEach(part =>
                part.ForEach(item => result.Add(selector.Invoke(item))));
            return result;
        }

        public static IList<T> CreateList<T>(Func<T> factory, int n = 10)
        {
            var list = new List<T>();
            for (var i = 0; i < n; i++)
                list.Add(factory.Invoke());

            return list;
        }
        /// <summary>
        /// Разбиваем коллекцию на коллекцию подколлекций указанного размера
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="partitionSize"></param>
        /// <returns></returns>
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> items, int partitionSize)
        {
            return items.Select((item, inx) => new { item, inx })
                .GroupBy(x => x.inx / partitionSize)
                .Select(g => g.Select(x => x.item));
        }

        public static bool Contains<T>(this IEnumerable<T> collection, Func<T, bool> predicate)
        {
            var find = collection.FirstOrDefault(predicate);

            return find != null;
        }

        public static IEnumerable<T> UnionAll<T>(this IEnumerable<IEnumerable<T>> chunks)
        {
            var result = new List<T>();

            chunks.ForEach(chunk =>
            {
                result.AddRange(chunk);
            });
            return result;
        }

        public static void UpdateRange<T>(this ConcurrentBag<T> bag, IEnumerable<T> newElemenents)
        {
            bag.Clear();
            newElemenents.ForEach(bag.Add);
        }
    }
}
