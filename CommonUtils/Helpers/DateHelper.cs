﻿using System;

namespace CommonUtils.Helpers
{
    public static class DateHelper
    {
        public static long GetTimestamp(this DateTime date)
        {
            return ((DateTimeOffset)date).ToUnixTimeSeconds();
        }

        public static long GetTimestampOrZero(this TimeSpan timespan)
        {
            var totalSeconds = (int)timespan.TotalSeconds;
            return totalSeconds >= 0 ? totalSeconds : 0;
        }

        public static string GetStringInfo(this TimeSpan timespan)
        {
            var seconds = timespan.Seconds;
            var totalMinutes = (int)timespan.TotalMinutes;

            return $"{totalMinutes}m {seconds}s";
        }
    }
}
