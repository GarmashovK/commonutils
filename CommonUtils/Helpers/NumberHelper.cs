﻿using System;
using System.Globalization;

namespace CommonUtils.Helpers
{
    public static class NumberHelper
    {
        public static bool IsEqual(this decimal one, decimal expected, int precision = 10)
        {
            var first = Math.Round(one, precision);
            var second = Math.Round(expected, precision);

            return first == second;
        }

        public static bool IsInRange(this decimal value, decimal min, decimal max)
        {
            return value >= min && value <= max;
        }

        public static decimal RoundWithPrecision(this decimal value, int precision = 6)
        {
            return Math.Round(value, precision);
        }

        public static bool IsValidCommission(this decimal value)
        {
            return value >= 0 && value < 1;
        }

        public static string ToStringWithClearEndNulls(this decimal value)
        {
            if (value == 0) return "0";
            var str = value.ToString(CultureInfo.InvariantCulture);

            return str.Contains('.') ?
                str.TrimEnd('0').TrimEnd('.') :
                str;
        }
    }
}
