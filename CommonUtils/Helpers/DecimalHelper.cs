﻿using System;
using System.Globalization;

namespace CommonUtils.Helpers
{
    public static class DecimalHelper
    {
        public static bool IsEqual(this decimal one, decimal expected, int precision = 10)
        {
            var first = Math.Round(one, precision);
            var second = Math.Round(expected, precision);

            return first == second;
        }

        public static bool IsInRange(this decimal value, decimal min, decimal max)
        {
            return value >= min && value <= max;
        }


        public static bool IsValidCommission(this decimal value)
        {
            return value >= 0 && value < 1;
        }
    }
}
