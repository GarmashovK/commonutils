﻿using System.Collections.Generic;

namespace Algorithms.Helpers
{
    public static class TypeHelper
    {
        public static bool IsDefault<T>(this T obj)
        {
            if (obj.IsValueType())
                return false;
            return EqualityComparer<T>.Default.Equals(obj, default(T));
        }

        public static bool IsNotDefault<T>(this T obj)
        {
            return obj.IsDefault() == false;
        }

        public static bool IsValueType<T>(this T obj)
        {
            return typeof(T).IsValueType;
        }
    }
}