﻿using System.Xml;

namespace CommonUtils.Helpers
{
    public static class XmlDocumentHelper
    {
        public static XmlElement CreateElementWithValue(this XmlDocument document, string name, string value)
        {
            var element = document.CreateElement(name);
            element.AppendChild(document.CreateTextNode(value));
            return element;
        }
    }
}