﻿using System;
using System.Collections;
using System.Linq;
using CommonUtils.Helpers;

namespace CommonUtils.Common
{
    /// <summary>
    /// Контракты кода
    /// </summary>
    public static class CodeContract
    { 
        public static void Assume<TException>(bool predicate, string message) where TException : Exception, new()
        {
            Requires<TException>(predicate, message);
        }

        public static void Requires<TException>(bool predicate, string message) where TException : Exception, new()
        {
            if (predicate) return;

            TException exp;

            var ctor = typeof(TException).GetConstructor(new[] { typeof(string) });
            if (ctor != null)
                exp = (TException)ctor.Invoke(new object[] { message });
            else
                exp = new TException();

            throw exp; 
        }

        public static void RequiresNotEmptyString(string str, string message)
        {
            Requires<ArgumentNullException>(str.IsNotNullOrWhiteSpace(), message);
        }

        public static void RequiresNotNull(object obj, string message)
        {
            Requires<ArgumentNullException>(obj != null, message);
        }

        public static bool ForAll(this IEnumerable objects, Func<object, bool> func)
        {
            return objects.Cast<object>().All(obj => func(obj) != false);
        }
    }
}
