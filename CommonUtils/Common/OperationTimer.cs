﻿using CommonUtils.Common.Logger;
using System;
using System.Diagnostics;

namespace CommonUtils.Common
{
    public sealed class OperationTimer : IDisposable
    {
        private readonly ILogger logger;

        private readonly Int32 m_collectionCount;
        private readonly Int64 m_startTime;
        private readonly String m_text;

        /// <summary>
        /// Временная оценка
        /// </summary>
        /// <param name="text"></param>
        /// <param name="flagGarbageCollector">Запустить сборщик мусора</param>
        public OperationTimer(String text, ILogger logger, bool flagGarbageCollector = false)
        {
            if (flagGarbageCollector)
                PrepareForOperation();
            m_text = text;
            m_collectionCount = GC.CollectionCount(0);
            // Это выражение должно быть последним в этом методе .
            // чтобы обеспечить максимально точную оценку быстродействия
            m_startTime = Stopwatch.GetTimestamp();
            this.logger = logger;
        }

        public void Dispose()
        {
            string message = string.Format("{0,6:###.00000 } sec (GCs={1,3}) {2}",
                                           (Stopwatch.GetTimestamp() - m_startTime) /
                                           (Double)Stopwatch.Frequency,
                                           GC.CollectionCount(0) - m_collectionCount, m_text);
            logger.Debug(message);
            Debug.WriteLine(message);
        }

        private static void PrepareForOperation()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
