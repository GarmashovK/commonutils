﻿using System;
using System.Threading;

namespace CommonUtils.Common
{
    public static class IdGenerator
    {
        // ReSharper disable once InconsistentNaming
        private static readonly object sync = new object();
        private const int Delay = 5;

        public static string GetNewOrderId()
        {
            var now = DateTime.Now;
            var counter = GetCounter() % 10000;

            var stringId = $"{now:yyMMdd}{counter:D4}";
            return stringId;
        }

        private static int GetCounter()
        {
            lock (sync)
            {
                Thread.Sleep(Delay);
                var passedFromDayStart = GetFromDayStartTimeSpan();

                return (int)passedFromDayStart.TotalMilliseconds / Delay;
            }
        }

        private static TimeSpan GetFromDayStartTimeSpan()
        {
            var now = DateTime.Now;
            var today = new DateTime(now.Year, now.Month, now.Day);

            return now - today;
        }
    }
}
