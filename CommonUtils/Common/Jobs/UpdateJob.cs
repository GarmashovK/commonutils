﻿using System;
using System.Collections.Generic;
using CommonUtils.Common.Extensions;
using CommonUtils.Common.Jobs.Interfaces;
using CommonUtils.Common.Logger;
using MoreLinq.Extensions;

namespace CommonUtils.Common.Jobs
{
    public class UpdateJob : JobActionScheduled
    {
        private readonly IEnumerable<IUpdateModule> updateModules;

        public UpdateJob(
            IEnumerable<IUpdateModule> updateModules,
            TimeSpan interval,
            TimeSpan startDelay, 
            ILogger logger) 
            : base(interval, startDelay, logger)
        {
            CodeContract.Requires<ArgumentNullException>(updateModules != null, nameof(updateModules));
            CodeContract.Requires<ArgumentNullException>(updateModules.ForAll(mod => mod != null), nameof(updateModules));
            this.updateModules = updateModules;
        }

        protected override Action GetAction()
        {
            return UpdateAction;
        }

        private void UpdateAction()
        {
            updateModules.ForEach(module => 
                Safe.Do(logger, module.Update));
        }
    }
}