﻿using System;
using CommonUtils.Common.Logger;

namespace CommonUtils.Common.Jobs
{
    /// <summary>
    /// Периодическая задача c передаваемым методом на выполнение
    /// </summary>
    public class JobActionScheduled : JobBaseScheduled
    {
        /// <summary>
        /// Дискретность вызовов
        /// </summary>
        private readonly TimeSpan interval;

        public JobActionScheduled(TimeSpan interval, TimeSpan startDelay, ILogger logger) : base(startDelay, logger)
        {
            this.interval = interval;
        }

        public JobActionScheduled(Action action, TimeSpan interval, TimeSpan startDelay, ILogger logger) : base(action, startDelay, logger)
        {
            this.interval = interval;
        }

        protected override TimeSpan GetDelay()
        {
            return interval;
        }
    }
}
