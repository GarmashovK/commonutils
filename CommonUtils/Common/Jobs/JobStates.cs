﻿namespace CommonUtils.Common.Jobs
{
    public enum JobStates
    {
        None,
        Stopped,
        Paused,
        Waiting,
        InProgress,
        Finalize
    }
}
