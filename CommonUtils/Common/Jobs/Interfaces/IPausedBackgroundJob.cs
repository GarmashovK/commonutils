﻿namespace CommonUtils.Common.Jobs.Interfaces
{
    public interface IPausedBackgroundJob : IBackgroundJob
    {
        void Pause();
        void Unpause();
    }
}