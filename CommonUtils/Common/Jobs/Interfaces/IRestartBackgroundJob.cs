﻿using System.Threading.Tasks;

namespace CommonUtils.Common.Jobs.Interfaces
{
    public interface IRestartBackgroundJob
    {
        Task Restart();
    }
}