﻿namespace CommonUtils.Common.Jobs.Interfaces
{
    public interface IUpdateModule
    {
        void Update();
    }
}