﻿using System.Threading.Tasks;

namespace CommonUtils.Common.Jobs.Interfaces
{
    /// <summary>
    /// Интерфейс фоновой задачи 
    /// </summary>
    public interface IBackgroundJob
    {
        /// <summary>
        /// Состояние задачи
        /// </summary>
        JobStates JobState { get;}

        /// <summary>
        /// Запуск выполнения задачи.
        /// Возвращаемый Task позволяет ждать выполнение
        /// однократно выполняемой задачи
        /// </summary>
        /// <returns></returns>
        Task Start();

        void Stop();
    }
}
