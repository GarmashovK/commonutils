﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CommonUtils.Common.Jobs.Interfaces;
using CommonUtils.Common.Logger;

namespace CommonUtils.Common.Jobs
{
    /// <summary>
    /// Потокобезопасный базовый класс 
    /// периодических и однократно выполняемых задач
    /// </summary>
    public abstract class JobBaseScheduled : IPausedBackgroundJob, IDisposable
    {
        /// <summary>
        /// Состояние задачи
        /// </summary>
        public JobStates JobState { get; private set; }

        protected CancellationTokenSource cancelToken = new CancellationTokenSource();
        private readonly Action action;
        protected readonly TimeSpan startDelay;
        private readonly TimeSpan nextDelay = TimeSpan.MaxValue;
        protected readonly ILogger logger;

        protected JobBaseScheduled(TimeSpan startDelay, ILogger logger)
        {
            CodeContract.RequiresNotNull(logger, nameof(logger));

            this.startDelay = startDelay;
            this.logger = logger;
        }

        protected JobBaseScheduled(Action action, TimeSpan startDelay, ILogger logger) 
            : this(startDelay, logger)
        {
            CodeContract.Requires<ArgumentNullException>(action != null, nameof(action));
            this.action = action;
        }

        /// <summary>
        /// Переопределение этого метода позволяет наследникам
        /// указывать свой метод action
        /// </summary>
        /// <returns></returns>
        protected virtual Action GetAction()
        {
            return action;
        }

        /// <summary>
        /// Задержка перед следующей итерацией
        /// Переопределение этого метода делает задачу периодической
        /// </summary>
        /// <returns></returns>
        protected virtual TimeSpan GetDelay()
        {
            return nextDelay;
        }

        private readonly object sync = new object();
        private void DoAction()
        {
            lock (sync)
            {
                try
                {
                    while (cancelToken.IsCancellationRequested == false)
                    {
                        if (JobState != JobStates.Paused)
                        {
                            JobState = JobStates.InProgress;
                            try
                            {
                                GetAction().Invoke();
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.ToString());
                            }
                        }

                        // если однократно выполняемая задача
                        if (GetDelay() == TimeSpan.MaxValue || cancelToken.IsCancellationRequested)
                        {
                            JobState = JobStates.Finalize;
                            return;
                        }

                        JobState = JobStates.Waiting;
                        var delay = GetDelay();
                        if (delay <= TimeSpan.Zero)
                        {
                            var error = "JobBaseScheduled: delay <= TimeSpan.Zero!";
                            logger.Error(error);
                            throw new Exception(error);
                        }
                        Thread.Sleep(delay);
                    }
                }
                catch (AggregateException ex)
                {
                    logger.Error(ex.Flatten().InnerExceptions[0].Message);
                }
                finally
                {
                    JobState = JobStates.Finalize;
                }
            }
        }

        public virtual Task Start()
        {
            JobState = JobStates.InProgress;
            var task = new Task(t =>
            {
                Thread.Sleep(startDelay);
                DoAction();
            },cancelToken.Token);
            task.ConfigureAwait(false);
            task.Start();
            return task;
        }

        public virtual void Stop()
        {
            JobState = JobStates.Stopped;
            cancelToken.Cancel();
        }

        public void Pause()
        {
            JobState = JobStates.Paused;
        }

        public void Unpause()
        {
            JobState = JobStates.Waiting;
        }

        public void Dispose()
        {
            cancelToken.Dispose();
        }
    }
}
