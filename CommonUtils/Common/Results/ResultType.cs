﻿using System.Runtime.Serialization;

namespace CommonUtils.Common.Results
{
    public enum ResultType
    {
        [EnumMember(Value = "error")]
        Error,
        [EnumMember(Value = "success")]
        Success,
        Warning,
        Info,
        Empty
    }
}
