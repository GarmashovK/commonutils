﻿namespace CommonUtils.Common.Results
{
    public interface IResult
    {
        ResultType Type { get; }
        string Message { get; }
    }
}
