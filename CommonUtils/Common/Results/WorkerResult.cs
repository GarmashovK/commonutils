﻿using System.Collections.Generic;
using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CommonUtils.Common.Results
{
    public class Result : IResult
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public ResultType Type { get; protected set; }
        public string Message { get; protected set; }
        public Dictionary<string, string> Warnings = null;

        protected Result()
        {
            Message = string.Empty;
            Type = ResultType.Success;
        }

        protected Result(ResultType type, string message = null)
        {
            Type = type;
            Message = message;
        }

        public static Result Success()
        {
            return new Result();
        }

        public static Result Error()
        {
            return new Result(ResultType.Error);
        }

        public static Result Error(string message)
        {
            return new Result(ResultType.Error, message);
        }

        public static Result Warning(IReadOnlyDictionary<string, string> warnings)
        {
            var warningResult = new Result(ResultType.Warning);
            warnings.ForEach(w => warningResult.Warnings.Add(w.Key, w.Value));
            return warningResult;
        }

        public static Result<T> Error<T>()
        {
            return new Result<T>(string.Empty, ResultType.Error);
        }

        public static Result<T> Error<T>(string error)
        {
            return new Result<T>(error, ResultType.Error);
        }

        public static Result<T> Error<T>(T result)
        {
            return new Result<T>(result, ResultType.Error);
        }

        public static Result<T> Warning<T>(T result)
        {
            return new Result<T>(result, ResultType.Warning);
        }
        
        public static Result<T> Success<T>(T result)
        {
            return new Result<T>(result);
        }

        public static Result<T> Success<T>(T result, string message)
        {
            return new Result<T>(result,message);
        }

        public static Result SuccessMessage(string message)
        {
            return new Result(ResultType.Success, message);
        }

        public bool IsSuccess() => Type == ResultType.Success;

        public bool IsWarning() => Type == ResultType.Warning;

        public bool IsFailed() => Type == ResultType.Error;
    }

    /// <summary>
    /// Результат шага работы бизнес-логики
    /// </summary>
    public class Result<T> : Result
    {
        public T Data { get; }

        public Result(ResultType type)
        {
            Type = type;
        }

        public Result(string message, ResultType type)
        {
            Data = default(T);
            Type = type;
            Message = message;
        }

        public Result(T result, string message, ResultType type)
        {
            Data = result;
            Message = message;
            Type = type;
        }

        public Result(T entity, ResultType type)
        {
            Data = entity;
            Type = type;
        }

        public Result(T entity, string message)
        {
            Data = entity;
            Message = message;
            Type = ResultType.Success;
        }

        public Result(T entity)
        {
            Data = entity;
        }

        public T GetEntity()
        {
            return Data;
        }

        public new static Result<T> Error(string message)
        {
            return new Result<T>(default(T))
            {
                Type = ResultType.Error,
                Message = message
            };
        }

    }
}
