﻿namespace CommonUtils.Common
{
    public class ThreadSafeItem<T>
    {
        private T item;
        public T Item {
            get {
                lock (sync)
                {
                    return item;
                }
            }

            set {
                lock (sync)
                {
                    item = value;
                }
            }
        }

        private object sync = new object();

        private ThreadSafeItem() { }

        public ThreadSafeItem(T initItem)
        {
            item = initItem;
        }
    }
}
