﻿using System;

namespace CommonUtils.Common.Cache
{
    public abstract class BaseLifetimeCache<T> : ICache<T>
    {
        protected T cachedElem;
        public abstract bool NeedToUpdate();

        public abstract void Update(T newCache);

        protected readonly object sync = new object();

        public virtual T GetCache()
        {
            if (NeedToUpdate())
                throw new Exception("Need to update!");
            lock (sync)
            {
                return cachedElem;
            }
        }
    }
}