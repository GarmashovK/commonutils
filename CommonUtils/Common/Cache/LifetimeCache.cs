﻿using System;

namespace CommonUtils.Common.Cache
{
    public class LifetimeCache<T> : BaseLifetimeCache<T>
    {
        private readonly TimeSpan lifeTime;

        public LifetimeCache(TimeSpan lifeTime)
        {
            this.lifeTime = lifeTime;
        }

        public override bool NeedToUpdate()
        {
            return DateTime.Now - LastUpdate > lifeTime;
        }

        private DateTime LastUpdate = DateTime.MinValue;
        public override void Update(T obj)
        {
            lock (sync)
            {
                LastUpdate = DateTime.Now;
                cachedElem = obj;
            }
        }
    }
}
