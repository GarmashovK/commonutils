﻿namespace CommonUtils.Common.Cache
{
    public interface ICache<T> 
    {
        bool NeedToUpdate();
        void Update(T newCache);
        T GetCache();
    }
}