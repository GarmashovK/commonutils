﻿namespace CommonUtils.Common.Cache
{
    public class CountLifetimeCache<T> : BaseLifetimeCache<T>
    {
        private readonly int maxUse;
        private int useCount = 0;
        private object sync = new object();

        public CountLifetimeCache(int maxUse)
        {
            this.maxUse = maxUse;
        }

        public override bool NeedToUpdate()
        {
            lock (sync)
            {
                return useCount < maxUse;
            }
        }

        public override void Update(T newCache)
        {
            lock (sync)
            {
                useCount = 0;
                cachedElem = newCache;
            }
        }

        public override T GetCache()
        {
            lock (sync)
            {
                useCount++;
            }

            return base.GetCache();
        }
    }
}