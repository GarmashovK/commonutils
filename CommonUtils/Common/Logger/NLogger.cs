﻿using System;

namespace CommonUtils.Common.Logger
{
    /// <summary>
    /// Класс логирования через NLog
    /// </summary>
    public class NLogger : ILogger
    {
        private readonly NLog.Logger Logger;
        
        static NLogger()
        {
            NLog.LogManager.Configuration.Reload();
        }

        public NLogger()
        {
            Logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public NLogger(string name)
        {
            Logger = NLog.LogManager.GetLogger(name);
        }

        public void Debug(string msg)
        { 
            Logger.Debug(msg);
        }

        public void Debug(string msg, Exception ex)
        {
            Logger.Debug(ex, msg);
        }

        public void DebugFormat(string format, params object[] args)
        {
            Logger.Debug(format, args);
        }

        public void Error(string msg)
        {
            Logger.Error(msg);
        }

        public void Error(string msg, Exception ex)
        {
            Logger.Error(ex, msg);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            Logger.Error(format, args);
        }

        public void Fatal(string msg)
        {
            Logger.Fatal(msg);
        }

        public void Fatal(string msg, Exception ex)
        {
            Logger.Fatal(ex, msg);
        }

        public void FatalFormat(string format, params object[] args)
        {
            Logger.Fatal(format, args);
        }

        public void Info(string msg)
        {
            Logger.Info(msg);
        }

        public void Info(string msg, Exception ex)
        {
            Logger.Info(ex, msg);
        }

        public void InfoFormat(string format, params object[] args)
        {
            Logger.Info(format, args);
        }
    }
}
