﻿using System;

namespace CommonUtils.Common.Logger
{
    /// <summary>
    /// Интерфейс логгера
    /// </summary>
    public interface ILogger
    {
        #region Debug
        void Debug(string msg);
        void Debug(string msg, Exception ex);
        void DebugFormat(string format, params object[] args);
        #endregion

        #region Error
        void Error(string msg);
        void Error(string msg, Exception ex);
        void ErrorFormat(string format, params object[] args);
        #endregion

        #region Fatal
        void Fatal(string msg);
        void Fatal(string msg, Exception ex);
        void FatalFormat(string format, params object[] args);
        #endregion

        #region Info
        void Info(string msg);
        void Info(string msg, Exception ex);
        void InfoFormat(string format, params object[] args);
        #endregion
    }
}