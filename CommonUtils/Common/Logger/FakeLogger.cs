﻿using System;

namespace CommonUtils.Common.Logger
{
    /// <summary>
    /// Фейковый логгер для тестирования
    /// </summary>
    public class FakeLogger : ILogger
    {
        private static void Log(string msg)
        {
            System.Diagnostics.Debug.WriteLine(msg);
            Console.WriteLine(msg);
        }
        public void Debug(string msg)
        {
            Log(msg);
        }

        public void Debug(string msg, Exception ex)
        {
            Log(msg + " " + ex);
        }

        public void DebugFormat(string format, params object[] args)
        {
        }

        public void Error(string msg)
        {
            Log(msg);
        }

        public void Error(string msg, Exception ex)
        {
            Log(msg + " " + ex);
        }

        public void ErrorFormat(string format, params object[] args)
        {
        }

        public void Fatal(string msg)
        {
            Log(msg);
        }

        public void Fatal(string msg, Exception ex)
        {
            Log(msg + " " + ex);
        }

        public void FatalFormat(string format, params object[] args)
        {
        }

        public void Info(string msg)
        {
            Log(msg);
        }

        public void Info(string msg, Exception ex)
        {
            Log(msg + " " + ex);
        }

        public void InfoFormat(string format, params object[] args)
        {
        }
    }
}
