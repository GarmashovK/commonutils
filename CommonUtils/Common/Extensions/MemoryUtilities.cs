﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace CommonUtils.Common.Extensions
{

    namespace Bp.Common.Extensions
    {
        public static class MemoryUtilities
        {
            /// <summary>
            /// Nice way to calculate the size of managed object!
            /// </summary>
            /// <typeparam name="TT"></typeparam>
            private class Size<TT>
            {
                private readonly TT obj;
                private readonly HashSet<object> references;
                private static readonly int PointerSize = Environment.Is64BitOperatingSystem ? sizeof(long) : sizeof(int);
                public Size(TT obj)
                {
                    this.obj = obj;
                    references = new HashSet<object>() { this.obj };
                }
                public long GetSizeInBytes()
                {
                    return GetSizeInBytes(obj);
                }

                // The core functionality. Recurrently calls itself when an object appears to have fields 
                // until all fields have been  visited, or were "visited" (calculated) already.
                private long GetSizeInBytes<T>(T source)
                {
                    if (source == null) return sizeof(int);
                    var type = source.GetType();

                    if (type.IsPrimitive)
                    {
                        return SizePrimitiveType<T>(type);
                    }

                    if (source is decimal)
                    {
                        return sizeof(decimal);
                    }
                    if (source is string)
                    {
                        return sizeof(char) * source.ToString().Length;
                    }
                    if (type.IsEnum)
                    {
                        return sizeof(int);
                    }
                    if (type.IsArray)
                    {
                        long pointerSize = PointerSize;
                        var casted = (IEnumerable)source;
                        pointerSize += casted.Cast<object>().Sum(item => this.GetSizeInBytes(item));
                        return pointerSize;
                    }
                    if (source is Pointer)
                    {
                        return PointerSize;
                    }

                    var size = CalculateMemorySize(source, type);
                    return size;
                }

                private long CalculateMemorySize<T>(T source, Type type)
                {
                    long size = 0;
                    var t = type;
                    while (t != null)
                    {
                        size += PointerSize;
                        var fields = t.GetFields(BindingFlags.Instance | BindingFlags.Public |
                                                 BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
                        foreach (var field in fields)
                        {
                            var tempVal = field.GetValue(source);
                            if (references.Contains(tempVal)) continue;

                            references.Add(tempVal);
                            size += GetSizeInBytes(tempVal);
                        }
                        t = t.BaseType;
                    }
                    return size;
                }
                private static long SizePrimitiveType<T>(Type type)
                {
                    switch (Type.GetTypeCode(type))
                    {
                        case TypeCode.Boolean:
                        case TypeCode.Byte:
                        case TypeCode.SByte:
                            return sizeof(byte);
                        case TypeCode.Char:
                            return sizeof(char);
                        case TypeCode.Single:
                            return sizeof(float);
                        case TypeCode.Double:
                            return sizeof(double);
                        case TypeCode.Int16:
                        case TypeCode.UInt16:
                            return sizeof(Int16);
                        case TypeCode.Int32:
                        case TypeCode.UInt32:
                            return sizeof(Int32);
                        case TypeCode.Int64:
                        case TypeCode.UInt64:
                            return sizeof(Int64);
                    }
                    return sizeof(Int64);
                }
            }

            // The actual, exposed method:
            public static long SizeInBytes<T>(this T someObject)
            {
                var temp = new Size<T>(someObject);
                var tempSize = temp.GetSizeInBytes();
                return tempSize;
            }
        }
    }
}