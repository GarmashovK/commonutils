﻿using System;
using CommonUtils.Common.Logger;
using CommonUtils.Common.Results;

namespace CommonUtils.Common.Extensions
{
    public static class Safe
    {
        public static string GetFullExceptionsStackMessages(this Exception ex)
        {
            return ex.InnerException == null ? ex.Message : string.Join(", ", ex.Message, GetFullExceptionsStackMessages(ex.InnerException));
        }

        public static void Do(ILogger logger, Action action, string message = null)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception e)
            {
                if (message != null)
                    logger.Error(message);
                logger.Error(e.GetFullExceptionsStackMessages());
                logger.Error(e.StackTrace);
            }
        }

        public static T Do<T>(ILogger logger, Func<T> func, string message = null)
        {
            T result = default(T);
            try
            {
                result = func.Invoke();
            }
            catch (Exception e)
            {
                if (message != null)
                    logger.Error(message);
                logger.Error(e.GetFullExceptionsStackMessages());
                logger.Error(e.StackTrace);
            }
            return result;
        }

        public static Result<T> DoWithResult<T>(ILogger logger, Func<T> func, string message = null)
        {
            try
            {
                T result = func.Invoke();
                return Result.Success(result);
            }
            catch (Exception e)
            {
                if (message != null)
                    logger.Error(message);
                logger.Error(e.GetFullExceptionsStackMessages());
                logger.Error(e.StackTrace);
                return Result.Error<T>(e.Message);
            }

        }
        public static Result DoWithResult(ILogger logger, Action action, string message = null)
        {
            try
            {
                action.Invoke();
                return Result.Success();
            }
            catch (Exception e)
            {
                if (message != null)
                    logger.Error(message);
                logger.Error(e.GetFullExceptionsStackMessages());
                logger.Error(e.StackTrace);
                return Result.Error(e.Message);
            }

        }
    }
}
